from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse


class SystemTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        self.client_logged_in = Client()
        self.logged_in = self.client_logged_in.login(username='testuser', password='12345')
        self.assertEqual(self.logged_in, True)


    def test_general(self):
        response = self.client.get('/projects/')
        self.assertEqual(response.status_code, 200)

        response = self.client_logged_in.get('/projects/')
        self.assertEqual(response.status_code, 200)


    def test_messaging(self):
        # logged in
        response = self.client_logged_in.get('/messages/inbox/')
        self.assertEqual(response.status_code, 200)

        response = self.client_logged_in.get(reverse('postman:inbox'))
        self.assertEqual(response.status_code, 200)

        # not logged in, gets redirect to login
        response = self.client.get(reverse('postman:inbox'))
        self.assertEqual(response.status_code, 302)

