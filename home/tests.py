from django.test import TestCase
from django.contrib.auth.models import User

from projects.models import Project
from user.models import Profile
from home.views import home, get_given_offer_projects

# Create your tests here.
class HomeTestCase(TestCase):
    fixtures = ['fixtures/test-seed.json']

    def setUp(self):
        self.client.login(username='admin', password='qwerty123')

    def test_customer_projects_none(self):
        response = self.client.get("/")
        #Make sure they can still reach the home page
        self.assertEqual(response.status_code, 200)
        user_projects = response.context["customer_projects"]
        #make sure users without customer projects still have none
        self.assertEqual(len(user_projects),0)

    def test_customer_projects(self):
        #sign in as a user with customer projects
        self.client.login(username="harrypotter", password="qwerty123")
        #get the "home" page of the user
        response = self.client.get("/")
        #ensure a valid response is recieved
        self.assertEqual(response.status_code, 200)
        customer_projects = response.context["customer_projects"]
        #Test that both of the projects are found
        self.assertEqual(len(customer_projects), 2)
        current_user = response.context["user"].profile
        for p in customer_projects:
            #print(len(parts))
            #verify all customer projects are in progress
            self.assertEqual("i", p.status)
            #make sure the user is a participant in each of the projects returned
            self.assertTrue(current_user in p.participants.all())
