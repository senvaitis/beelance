from django.contrib.auth.models import User
from django.test import TestCase

from projects.models import TaskOffer, Task, Delivery, Team, TaskUserPermissions


class ProjectTestCase(TestCase):
    # automatically loaded
    fixtures = ['fixtures/test-seed.json']


    def setUp(self):
        self.client.login(username='admin', password='qwerty123')



    # ------------------------------------------------------------
    # Testing funtion: project_view
    # ------------------------------------------------------------

    # Checks if project owner can change project status from 'o' (open) to 'i' (in progress)
    def test_project_change_status(self):
        response = self.client.post('/projects/1/', {
            'status_change':'',
            'status':'i'})
        self.assertEqual(response.status_code, 200)
        project = response.context['project']
        self.assertEqual(project.status, 'i')


    # Checks if other user can submit a task offer
    def test_project_offer_submit(self):
        # other user for offer submitting
        self.client.login(username='harrypotter', password='qwerty123')

        response = self.client.post('/projects/1/', {
            'offer_submit':'',
            'taskvalue':'1',
            'title':'TestTitle',
            'description':'TestDescription',
            'price':'90000'})
        self.assertEqual(response.status_code, 200)
        task_offer = TaskOffer.objects.get(title='TestTitle')
        self.assertIsInstance(task_offer, TaskOffer)
        self.assertEqual(task_offer.status, 'p')
        self.assertEqual(task_offer.description, 'TestDescription')
        self.assertEqual(task_offer.price, 90000)
        self.assertEqual(task_offer.task_id, 1)


    # Checks if project owner can respond (accept) a task offer
    def test_project_offer_response(self):
        response = self.client.post('/projects/1/', {
            'offer_response': '',
            'taskofferid': '1',
            'status': 'a',
            'feedback': 'TestFeedback'})
        self.assertEqual(response.status_code, 200)
        task_offer = TaskOffer.objects.get(id=1)
        self.assertIsInstance(task_offer, TaskOffer)
        self.assertEqual(task_offer.status, 'a')
        self.assertEqual(task_offer.feedback, 'TestFeedback')
        self.assertEqual(task_offer.task_id, 1)



    # ------------------------------------------------------------
    # Testing funtion: get_user_task_permissions
    # ------------------------------------------------------------

    # Checks if owner of the task has proper permissions
    def test_task_permissions_for_task_owner(self):
        task = Task.objects.get(pk=1)
        user = User.objects.get(pk=1)
        user_permissions = task.get_user_task_permissions(user)
        self.assertEqual(user_permissions, TaskUserPermissions(
            write=True,
            read=True,
            modify=True,
            owner=True,
            upload=True))


    # Checks if the users which has the tasks beeen offered to have proper permissions
    def test_task_permissions_for_user_offered(self):
        task = Task.objects.get(pk=1)
        user = User.objects.get(pk=3)

        user_permissions = task.get_user_task_permissions(user)
        self.assertEqual(user_permissions, TaskUserPermissions(
            write=True,
            read=True,
            modify=True,
            upload=True))


    # Checks if the other users have proper permissions
    def test_task_permissions_for_other_user(self):
        task = Task.objects.get(pk=1)
        user = User.objects.get(pk=1)
        user2 = User.objects.get(pk=2)

        user_permissions = task.get_user_task_permissions(user2)
        self.assertEqual(user_permissions, TaskUserPermissions(view_task=True))



    # ------------------------------------------------------------
    # Testing funtion: task_view
    # ------------------------------------------------------------

    # Checks if a logged in user can view a task
    def test_task_view_get(self):
        # logged in
        self.client.login(username='harrypotter', password='qwerty123')
        response = self.client.get('/projects/1/tasks/1/')
        self.assertEqual(response.status_code, 200)
        # logged out
        self.client.logout()
        response = self.client.get('/projects/1/tasks/1/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/user/login/?next=/projects/1/tasks/1/')


    # Checks if a user can post a task delivery response
    def test_task_view_post_delivery_response(self):
        self.client.login(username='admin', password='qwerty123')
        response = self.client.post('/projects/2/tasks/2/', {
            'delivery-response':'',
            'delivery-id':'1',
            'status':'a',
            'feedback':'feedback1'})
        self.assertEqual(response.status_code, 200)
        delivery = Delivery.objects.get(id=1)
        self.assertIsInstance(delivery, Delivery)
        self.assertEqual(delivery.responding_user.user.username, 'admin')
        self.assertEqual(delivery.status, 'a')
        self.assertEqual(delivery.feedback, 'feedback1')
        self.assertEqual(delivery.task_id, 2)


    # Checks if a user can post a new team
    def test_task_view_post_new_team(self):
        self.client.login(username='harrypotter', password='qwerty123')
        response = self.client.post('/projects/2/tasks/2/', {
            'team':'',
            'name':'Team1'})
        self.assertEqual(response.status_code, 200)
        team = Team.objects.get(id=2)
        self.assertIsInstance(team, Team)
        self.assertEqual(team.name, 'Team1')


    # Checks if a user can add a new member to a team
    def test_task_view_post_a_new_member(self):
        self.client.login(username='harrypotter', password='qwerty123')
        response = self.client.post('/projects/1/tasks/1/', {
            'team-id': 1,
            'team-add': '',
            'members': '2'})
        team = Team.objects.get(id=1)
        self.assertIsInstance(team, Team)
        self.assertEqual(team.name, 'Gardening')
        self.assertEqual(response.status_code, 200)



    # ------------------------------------------------------------
    # Testing funtion: task_permissions
    # ------------------------------------------------------------
    # Checks if a logged in user can view task permissions form
    def test_task_permissions_get(self):
        # logged in
        self.client.login(username='admin', password='qwerty123')
        response = self.client.get('/projects/1/tasks/1/permissions/')
        self.assertEqual(response.status_code, 200)
        # logged out
        self.client.logout()
        response = self.client.get('/projects/1/tasks/1/permissions/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/user/login/?next=/projects/1/tasks/1/permissions/')


